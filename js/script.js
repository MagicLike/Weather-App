let weather = {
    fetchLocation: function (city) {
        fetch("https://geocoding-api.open-meteo.com/v1/search?name="
            + city
            + "&count=1")
            .then((response) => response.json())
            .then((data) => this.convertLocation(data));
    },

    convertLocation: function (data) {
        const { latitude, longitude } = "data.0";
    },

    fetchWeather: function () {
        fetch("https://api.open-meteo.com/v1/forecast?latitude="
            + latitude
            + "&longitude="
            + longitude
            + "&hourly=temperature_2m,relativehumidity_2m,apparent_temperature,rain,showers,snowfall&timezone=Europe%2FBerlin")
            .then((response) => response.json())
            .then((data) => console.log(data));
    },

    displayWeather: function () {

    }
}